import React, { useEffect, useState } from 'react'

import ChannelPane from './components/ChannelPane'
import DisconnectModal from './components/DisconnectModal'
import MemberPane from './components/MemberPane'
import MessagePane from './components/MessagePane'
import ws from './utils'

function App () {
  // Setup state
  const [messages, setMessages] = useState([])
  const [members, setMembers] = useState([])
  const [recipientNickname, setRecipientNickname] = useState('')
  const [activeChannel, setActiveChannel] = useState('general')
  const [channels, setChannels] = useState([])
  const [showNewChannel, setShowNewChannel] = useState(false)
  const [newChannel, setNewChannel] = useState()

  // Set ws callbacks
  useEffect(() => {
    ws.onclose = e => {
      const el = document.getElementById('disconnectModal')
      const modal = new window.bootstrap.Modal(el)
      modal.show()

      setMembers([])
    }
    ws.onmessage = e => {
      const message = JSON.parse(e.data)
      const { members, messageBody, senderNickname, type } = message
      const newRecipientNickname = message.recipientNickname
      const paneElement = document.getElementById('messageList')

      // Update state
      setChannels([...message.channels])
      setActiveChannel(message.activeChannel)
      setMessages([...messages, {
        type,
        senderNickname,
        messageBody
      }])
      setMembers(members)
      setRecipientNickname(newRecipientNickname)

      // Scroll to bottom of the messages pane
      paneElement.scrollTop = paneElement.scrollHeight
    }
    ws.onerror = e => {
      console.log('onerror')
      throw new Error('WebSocket error', e)
    }
    function sendMessage () {
      const elem = document.getElementById('messageInput')
      if (ws.readyState === WebSocket.OPEN) {
        ws.send(elem.value)
      }
      elem.value = ''
      elem.focus()
    }
    document.getElementById('chatForm').onsubmit = e => {
      e.preventDefault()
      sendMessage()
    }
  }) // end useEffect()

  // WS callack for updating activeChannel
  function handleJoinChannel (channel) {
    ws.send(`/join ${channel}`)
  }

  // State handlers
  function handleCreateNewChannel () {
    if (newChannel && (channels.indexOf(newChannel) === -1)) {
      ws.send(`/chan ${newChannel}`)
      console.log(channels)
      toggleShowNewChannel()
    }
  }
  function handleNewChannelChange (e) {
    setNewChannel(e.target.value)
  }
  function toggleShowNewChannel () {
    setShowNewChannel(!showNewChannel)
  }

  return (
    <div className='container-fluid' style={{ maxWidth: '1000px' }}>
      <DisconnectModal />
      <h1>wsnr-chat</h1>
      <div className='row'>
        <ChannelPane
          channels={channels}
          activeChannel={activeChannel}
          handleJoinChannel={handleJoinChannel}
          handleNewChannelChange={handleNewChannelChange}
          handleCreateNewChannel={handleCreateNewChannel}
          toggleShowNewChannel={toggleShowNewChannel}
          showNewChannel={showNewChannel}
        />
        <MessagePane
          className='col-sm'
          messages={messages}
        />
        <MemberPane
          className='col-sm-auto'
          members={members}
          recipientNickname={recipientNickname}
        />
      </div>
    </div>
  )
}

export default App
