const host = process.env.NODE_ENV === 'development'
  ? 'ws://localhost:5000'
  : 'wss://wsnr-chat-server.herokuapp.com'
const ws = new WebSocket(host)

export default ws
