import React from 'react'

import Message from './Message'

function MessageList ({ messages }) {
  const listItems = messages.map((message, i) => (
    <li key={i} className='list-group-item'>
      <Message message={message} />
    </li>
  ))
  return (
    <ul id='messageList' className='list-group' style={{ overflowY: 'scroll', height: '300px' }}>
      {listItems}
    </ul>
  )
}

export default MessageList
