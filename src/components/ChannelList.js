import React from 'react'

function ChannelList ({
  activeChannel, channels, handleCreateNewChannel, handleJoinChannel,
  handleNewChannelChange, showNewChannel
}) {
  const formSubmit = (e) => {
    e.preventDefault()
    handleCreateNewChannel()
  }

  const listItems = channels.map((channel, i) => {
    const active = channel === activeChannel
      ? 'list-group-item-primary'
      : ''
    const classes = `
      ${active}
      channel
      list-group-item
      list-group-item-action
    `
    const el = (
      <button
        key={i}
        className={classes}
        onClick={() => { handleJoinChannel(channel) }}
      >
        # <span className='channel-name'>{channel}</span>
      </button>
    )

    return el
  })

  const newChannelItem = (
    <form className='list-group-item' onSubmit={formSubmit}>
      <div className='input-group'>
        <input
          autoFocus
          className='form-control'
          onChange={handleNewChannelChange}
          placeholder='Channel'
          type='text'
        />
        <button className='btn btn-outline-primary' type='submit'>
          Create
        </button>
      </div>
    </form>
  )

  return (
    <div className='list-group'>
      {listItems}
      {
        showNewChannel
          ? (newChannelItem)
          : ''
      }
    </div>
  )
}

export default ChannelList
