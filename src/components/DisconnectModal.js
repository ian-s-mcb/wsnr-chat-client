import React from 'react'

function DisconnectModal ({ message }) {
  // Callback for modal show when there is a server disconnect
  const reconnect = () => window.location.reload()

  return (
    <div className='modal fade' id='disconnectModal' tabIndex='-1' aria-labelledby='disconnectModalLabel' aria-hidden='true'>
      <div className='modal-dialog'>
        <div className='modal-content'>
          <div className='modal-header'>
            <h5 className='modal-title text-danger' id='disconnectModalLabel'>Lost connection to server</h5>
            <button type='button' className='btn-close' data-bs-dismiss='modal' aria-label='Close' />
          </div>
          <div className='modal-body'>
            <p>Reload the page to reconnect to server.</p>
            <p>
              Be advised that all previous messages will be lost after
              reloading.
            </p>
          </div>
          <div className='modal-footer'>
            <button type='button' className='btn btn-secondary' data-bs-dismiss='modal'>Close</button>
            <button type='button' className='btn btn-danger' onClick={reconnect}>Reconnect</button>
          </div>
        </div>
      </div>
    </div>
  )
}

export default DisconnectModal
