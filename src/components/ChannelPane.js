import React from 'react'

import ChannelList from './ChannelList'

function ChannelPane ({
  activeChannel, channels, handleCreateNewChannel, handleJoinChannel,
  handleNewChannelChange, showNewChannel, toggleShowNewChannel
}) {
  return (
    <div style={{ minWidth: '225px', maxWidth: '225px' }}>
      <div className='row'>
        <div className='h6 col-sm-10'>CHANNELS</div>
        <button
          className='col-sm-1'
          style={{ border: 'none' }}
          onClick={toggleShowNewChannel}
        >
          <div className='h6 text-end text-primary'>+</div>
        </button>
      </div>
      <ChannelList
        activeChannel={activeChannel}
        channels={channels}
        handleCreateNewChannel={handleCreateNewChannel}
        handleJoinChannel={handleJoinChannel}
        handleNewChannelChange={handleNewChannelChange}
        showNewChannel={showNewChannel}
      />
    </div>
  )
}

export default ChannelPane
