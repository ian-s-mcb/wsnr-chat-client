import React from 'react'

function MemberList ({ members, recipientNickname }) {
  const listItems = members.map((member, i) => (
    <li key={i} className={`list-group-item ${member === recipientNickname ? 'text-primary' : ''}`}>
      {member}
    </li>
  ))
  return (
    <ul className='list-group'>
      {listItems}
    </ul>
  )
}

export default MemberList
