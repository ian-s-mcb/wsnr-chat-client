import React from 'react'

import MessageList from './MessageList'

function MessagePane ({ className, messages }) {
  return (
    <div id='messagesPane' className={className}>
      <div className='h6'>MESSAGES</div>
      <MessageList messages={messages} />
      <form id='chatForm' className='row row-cols-sm-auto g-0 align-items-center'>
        <div className='col-12' style={{ flexGrow: 1 }}>
          <input
            id='messageInput'
            className='form-control'
            type='text'
            placeholder='Type message'
          />
        </div>
        <div className='col-12'>
          <button className='btn btn-primary' type='submit'>Send</button>
        </div>
      </form>
    </div>
  )
}

export default MessagePane
