import React from 'react'

function Message ({ message }) {
  let elem
  if (message.type === 'message') {
    elem = (
      <>
        <span className='text-info'>{`<${message.senderNickname}>`}</span>&nbsp;
        <span>{message.messageBody}</span>
      </>
    )
  } else {
    elem = <span className='text-secondary'>{message.messageBody}</span>
  }
  return elem
}

export default Message
