import React from 'react'

import MemberList from './MemberList'

function MemberPane ({ className, members, recipientNickname }) {
  return (
    <div className={className}>
      <div className='h6'>MEMBERS</div>
      <MemberList
        members={members}
        recipientNickname={recipientNickname}
      />
    </div>
  )
}

export default MemberPane
