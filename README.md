# wsnr-chat-client

The frontend of a barebones, IRC-like chat app.

The frontend uses [React][react], and in particular the hooks feature,
[create-react-app][create-react-app], and [Bootstrap][bootstrap]. The
app is hosted on [Heroku][heroku]. The Heroku app provisioning was based
on [these][heroku-node-and-react] [two][heroku-node-and-ws] examples
from the Heroku docs.

This project connects to the backend described [here][backend].

### [Demo][demo]

### Screencast
![Video screencast of the wsnr-chat app in action][screencast]

[react]: https://reactjs.org/
[create-react-app]: https://github.com/facebook/create-react-app
[bootstrap]: https://getbootstrap.com/
[heroku]: https://www.heroku.com/
[heroku-node-and-react]: https://github.com/mars/heroku-cra-node
[heroku-node-and-ws]: https://github.com/heroku-examples/node-ws-test
[backend]: https://gitlab.com/ian-s-mcb/wsnr-chat-server
[demo]: https://wsnr-chat-client.herokuapp.com/
[screencast]: https://i.imgur.com/PKg2Tv0.mp4
